
1.4.0 / 2018-07-08
==================

  * Change version number
  * Oops... Task #645 - Allow exporting the NN weights to other formats
  * Add missing translations Task #645 - Allow exporting the NN weights to other formats
  * Allow exporting in `Octave-1-L` format Task #645 - Allow exporting the NN weights to other formats
  * Fix GIF not being created Task #744 - No se puede guardar GIF
  * Merge branch 'master' of gitlab.com:riddler_arg/mrft
  * Upgrade to latest jdrawinlib
  * Upgrade to latest exp4j
  * Remove unused import Task #727 - Migrate to the latest version of LibAI
  * Add the latest version of LibAI
  * Adapt Most classes to latest LibAI Task #727 - Migrate to the latest version of LibAI
  * Adapt Examples to latest LibAI Task #727 - Migrate to the latest version of LibAI
  * Update README.md

1.1.0 / 2018-04-02
==================

  * Update to RiddlerArgentina
  * Merge pull request #6 from dktcoding/codacy-suggestions
  * Remove unused import
  * Make some attributes private
  * Remove some empty methods
  * Add Codacy badge
  * Add gitignore rule for data folder
  * Cleanup a bit using some static imports
  * Remove usages of deprecated API
  * Remove unused import
  * Bump to version 1.1.0
  * Inline some oneline methods
  * Remove unused ActionEvent
  * Use lambdas in GUIs
  * Move variable declarations to top
  * Remove unused constructor
  * Use lambda in AboutPanel
  * Some more cleaning up...
  * Use lambda expressions
  * Move variable declarations to top
  * Remove unused parameter
  * More formatting improvements
  * Don't reasign variables
  * Add default tag to switch...
  * Remove unused variable
  * Formatting improvements in some third party classes (Codacy)
  * Fix last commit
  * Remove some unused variables
  * Extract some functions to make EntryPoint clearer
  * Make some methods private
  * Silence Codacy warnings
  * Remove unused variables
  * Don't use fully qualified name
  * Avoid possible NullPointerException
  * Move declarations to top and use constructor
  * Remove unused import
  * Remove empty else{} and redundant if
  * Use new library
  * Update to the latest jDrawingLib
  * Add DS_Store rule in .gitignore
  * Merge pull request #5 from dktcoding/remove-sinc
  * Merge pull request #4 from dktcoding/change-variable-names
  * MainWindow rename variable
  * FunctionMonospacedRand rename variable
  * CustomFunction rename variable
  * Remove sinc()
  * Merge pull request #3 from dktcoding/update-libai
  * Update to the latest libai
  * Merge pull request #2 from dktcoding/minor-changes
  * Sinc function was integrated to libai
  * Remove functions that were integrated in exp4j
  * Use builtin math functions
  * Some style changes
  * Merge pull request #1 from dktcoding/add-some-docs
  * Remove commented code
  * Remove some System.out calls
  * Fix comment
  * Minor changes on MainWindow
  * Use backprop constants in Examples
  * Use plot constants in examples
  * Make plot types public
  * Add some docs to the Example class
  * Update .gitignore
  * Update to latest libai version
  * Add more libraries used to README.md
  * Add some text to ABOUT
  * Add missing translations
  * Actually save the config file
  * Adapt classes for latest exp4j
  * Update to latest version of exp4j
  * Uses aproppiate value for error plot array init
  * Add extra space in conf template
  * Remove default table models
  * Minor change in example loading
  * Add meaningful names to variables in MainWindow
  * Change the name of a panel
  * Update .gitignore
  * Translate config file to Spanish
  * Add comments to config file
  * Remove unused imports
  * Lots of layout fixes in MainWindow
  * Use some meaningful var names in AboutPanel
  * Add some missing translations
  * Use some significant variable names in dialogs
  * Update README.md
  * Update README
  * Upload libs
  * Update .gitignore so it doesn't ignore the lib folder
  * Ant script
  * Initial Code
  * Update README
  * Create Readme.md
  * Initial commit
