# mrft (mlp real function trainer)
Small GUI used for training MLP with real functions.

Libraries used
--------------
- [`libai`](https://github.com/kronenthaler/libai): MLP implementation
- [`exp4j`](https://gitlab.com/riddler_arg/exp4j): Populating datasets,
transformations and noise
- [`jDrawingLib`](https://gitlab.com/riddler_arg/jdrawinglib): Plotting functions,
and creating images and animations
- [`FileDrop`](http://iharder.sourceforge.net/current/java/filedrop/): Drag & Drop support
- [`Java ECG Generator`](http://www.mit.edu/~gari/CODE/ECGSYN/JAVA/APPLET2/ecgsyn/ecg-java/source.html): Synthetic ECG (training example)
- [`Spline`](https://source.android.com/): Error smoothing
- [`GNU Octave`](https://www.gnu.org/software/octave/): Not directly used, but lot's of testing were done thanks to 
  GNU Octave
- Others...

Objectives
----------
The main reason for this program was to allow students to play with MLPs with
real functions, tests different configurations, etc.

There have been some real life applications (some even "cool"), that will be
properly described later on.

Building
--------
Building is as simple as (assuming you have a JDK properly installed):
- `git clone git@gitlab.com:riddler_arg/mrft.git`
- `cd mrft`
- `ant build-all`
